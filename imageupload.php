<?php
$docroot = $_SERVER['DOCUMENT_ROOT'];
$page = "Upload";
$error = $_GET['a'];
include("$docroot/templates/header.php");

$referral = $_GET['refer'];
$productName = $_GET['product'];

$_SESSION['refer'] = $referral;
$_SESSION['prod'] = $productName;

?>

<!-- Image header -->
<div class="w3-display-container w3-container">
    <img src="https://ak7.picdn.net/shutterstock/videos/2034937/thumb/1.jpg" alt="Jeans" style="width:100%;height:200px">
    <div class="w3-display-topleft w3-text-white" style="padding:24px 48px">
        <h1>Upload Image</h1>
    </div>
</div>

<div class="w3-display-container w3-container w3-center">
    <hr/>
</div>

<div class="w3-display-container w3-container w3-center">
    <h3>Almost done!</h3>
    <p>You are almost done uploading your new product</p>
    <p>All that's left is uploading an image</p>

    <form action="/action/uploadproduct" method="post" enctype="multipart/form-data">
        Select image to upload:
        <input type="file" name="fileToUpload" id="fileToUpload">
        <br/><br/>
        <input type="submit" value="Upload Image" class="w3-button w3-hover-black w3-yellow" name="submit">
    </form>

</div>

<?php 
include("$docroot/templates/footer.php");
?>  