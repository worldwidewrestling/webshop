<?php 
session_start();
$docroot = $_SERVER['DOCUMENT_ROOT'];
include("$docroot/includes/config.php");

// Retrieve preferred credentials
$userName = $_POST['username'];
$userPass = $_POST['pass'];
$product = $_POST['product'];

$query = "SELECT * FROM `users` WHERE `username` = '$userName'";
$sql = $con->query($query);
while($row = $sql->fetch_assoc())
{
    $userId = $row['id'];
    $userPassDB = $row['pass'];
    $verifyPassword = password_verify($userPass,$userPassDB);

    if($verifyPassword)
    {
        $_SESSION['id'] = $userId;
        $_SESSION['logged_in'] = true;
        header('location: ../shop/order?id='.$product.'');
    }

    else
    {
        header('location: ../error?a=invalidpass');
    }

}