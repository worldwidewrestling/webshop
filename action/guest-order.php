<?php
session_start();
include('../Includes/config.php');
$user = $_SESSION['id'];
if($_SESSION['id'] == NULL ) {
  $user = "guest";
}
$address = $_POST['address'];
$zip = $_POST['zip'];
$city = $_POST['city'];
$state = $_POST['state'];
$name = $_POST['name'];
$country = $_POST['country'];
$size = $_POST['size'];
$product = $_SESSION['product'];
$full_name = $_POST['name'];

$query = "SELECT * FROM `products` WHERE `id` = '$product'";
$sql = mysqli_query($con,$query);
while($row = mysqli_fetch_assoc($sql)) {
    $product_name = $row['name'];
    $product_price = $row['price'];
}

$length = '8';
$characters = '0123456789';
$string = '';

for ($i = 0; $i < $length; $i++) {
    $string .= $characters[mt_rand(0, strlen($characters) - 1)];
}
$query3 = "INSERT INTO `orders` (`id`,`name`,`address`,`zip`,`state`,`country`,`product`,`status`,`size`,`city`) VALUES ('$string','$name','$address','$zip','$state','$country','$product_name','0','$size','$city')";
$sql3 = mysqli_query($con,$query3);

$_SESSION['order_id'] = $string;

?>

<?php include('../Templates/header.php'); ?>

<div class="w3-display-container w3-container">
    <img src="https://ak7.picdn.net/shutterstock/videos/2034937/thumb/1.jpg" alt="Jeans" style="width:100%;height:200px">
    <div class="w3-display-topleft w3-text-white" style="padding:24px 48px">
        <h1>Payment for your order</h1>
        <p><?php echo "$message"; ?></p>
    </div>
</div>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    // Configure environment
    env: 'production',
    client: {
      sandbox: 'AawbhcHv2J_xDnq1V_GTOkUtsjeqLGnRF9mdcpK5ccWsgFeRM1SXPIVfEhVF24fnD92Qq-s9_zw1A8Mr',
      production: 'AQb03CMcHVEnLmm78ngheO7wcZqipgZv4Q8nkyLpfmu3vP1p4GAFZ-BEPIW5RgdNqhJm67GGtE67rscd'
    },
    // Customize button (optional)
    locale: 'en_US',
    style: {
      size: 'small',
      color: 'gold',
      shape: 'pill',
    },

    // Enable Pay Now checkout flow (optional)
    commit: true,

    // Set up a payment
    payment: function(data, actions) {
      return actions.payment.create({
        transactions: [{
          amount: {
            total: '<?php echo "$product_price"; ?>',
            currency: 'USD'
          }
        }]
      });
    },
    // Execute the payment
    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function() {
        // Show a confirmation message to the buyer
        window.location.href = "http://shop.weare3dub.com/confirm";
      });
    }
  }, '#paypal-button');

</script>

<div class="w3-display-container w3-container w3-center w3-margin-bottom w3-margin-top">
    <div id="paypal-button"></div>
</div>

<?php include('../Templates/footer'); ?>