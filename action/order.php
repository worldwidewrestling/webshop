<?php
session_start();
include('../Includes/config.php');

$get_paypal_data = "SELECT * FROM `paypal`";
$get2 = mysqli_query($con,$get_paypal_data);
while($row3 = mysqli_fetch_assoc($get2)){
  $paypal_live = $row3['live'];
  $paypal_sandbox = $row3['sandbox'];
  $paypal_method = $row3['method'];
}

$user = $_SESSION['id'];
if($_SESSION['id'] == NULL ) {
  $user = "guest";
}

$get_premium = "SELECT * FROM `users` WHERE `id` = '$user'";
$get = mysqli_query($con,$get_premium);
while($row2 = mysqli_fetch_assoc($get)) {
  $premium = $row2['premium'];
}

$address = $_POST['address'];
$zip = $_POST['zip'];
$city = $_POST['city'];
$state = $_POST['state'];
$name = $_POST['name'];
$country = $_POST['country'];
$size = $_POST['size'];
$product = $_SESSION['product'];
$coupon = $_POST['coupon'];
$full_name = $_POST['name'];
$order_date = date('Y-m-d');

$query = "SELECT * FROM `products` WHERE `id` = '$product'";
$sql = mysqli_query($con,$query);
while($row = mysqli_fetch_assoc($sql)) {
    $product_name = $row['name'];
    $product_price = $row['price'];
}

$query2 = "SELECT * FROM `coupons` WHERE `code` = '$coupon' AND`status` = '0'";
$sql2 = mysqli_query($con,$query2);
$result = mysqli_num_rows($sql2);
if($result > 0) {
    $redeem = "UPDATE `coupons` SET `status` = '1' WHERE `code` = '$coupon'";
    $redeem2 = mysqli_query($con,$redeem);
    if($redeem) {
        $message = "The coupon was redeemed";
        $new_price = 0.95 * $product_price;
        if($premium == '1') {
          $new_price2 = $new_price * 0.95;
        } else { 
          $new_price2 = $new_price;
        }
    }
} else {
    $message = "No coupon was found";
    $new_price = $product_price;
    if($premium == '1') {
      $new_price2 = $new_price * 0.95;
    } else {
      $new_price2 = $new_price;
    }
}
$length = '8';
$characters = '0123456789';
$string = '';

for ($i = 0; $i < $length; $i++) {
    $string .= $characters[mt_rand(0, strlen($characters) - 1)];
}
$query3 = "INSERT INTO `orders` (`id`,`name`,`address`,`zip`,`state`,`country`,`product`,`status`,`user_id`,`size`,`city`,`amount`,`date`) VALUES ('$string','$name','$address','$zip','$state','$country','$product_name','0','$user','$size','$city','$new_price2','$order_date')";
$sql3 = mysqli_query($con,$query3);

$_SESSION['order_id'] = $string;

?>

<?php include('../Templates/header.php'); ?>

<div class="w3-display-container w3-container">
    <img src="https://ak7.picdn.net/shutterstock/videos/2034937/thumb/1.jpg" alt="Jeans" style="width:100%;height:200px">
    <div class="w3-display-topleft w3-text-white" style="padding:24px 48px">
        <h1>Payment for your order</h1>
        <p><?php echo "$message"; ?></p>
    </div>
</div>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    // Configure environment
    env: '<?php echo "$paypal_method"; ?>',
    client: {
      sandbox: '<?php echo "$paypal_sandbox"; ?>',
      production: '<?php echo "$paypal_live"; ?>'
    },
    // Customize button (optional)
    locale: 'en_US',
    style: {
      size: 'small',
      color: 'gold',
      shape: 'pill',
    },

    // Enable Pay Now checkout flow (optional)
    commit: true,

    // Set up a payment
    payment: function(data, actions) {
      return actions.payment.create({
        transactions: [{
          amount: {
            total: '<?php echo "$new_price2"; ?>',
            currency: 'USD'
          }
        }]
      });
    },
    // Execute the payment
    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function() {
        // Show a confirmation message to the buyer
        window.location.href = "http://shop.weare3dub.com/confirm";
      });
    }
  }, '#paypal-button');

</script>

<div class="w3-display-container w3-container w3-center w3-margin-bottom w3-margin-top">
    <div id="paypal-button"></div>
</div>

<?php include('../Templates/footer'); ?>