<?php 

class Account
{
    function registerAccount($userName,$userPass)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        $userPassDB = password_hash($userPass,PASSWORD_DEFAULT);

        $query = "INSERT INTO `users` (`username`,`pass`) VALUES ('$userName','$userPassDB')";
        $sql = $con->query($query);
        if($sql)
        {
            return "OK";
        } 
        else 
        {
            return "Error";
        }
    }

    function accountLogin($userName,$userPass)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        $query = "SELECT * FROM `users` WHERE `username` = '$userName'";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $userId = $row['id'];
            $userPassDB = $row['pass'];
            $verifyPassword = password_verify($userPass,$userPassDB);

            if($verifyPassword)
            {
                $_SESSION['id'] = $userId;
                $_SESSION['logged_in'] = true;
                return "OK";
            }

            else
            {
                return "Error";
            }

        }

    }

}

?>