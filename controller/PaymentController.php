<?php 

class Payment
{
    function createOrder($method,$orderPrice,$orderPayer,$orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        if($method == "sandbox")
        {
            $query = "SELECT * FROM `paypal`";
            $sql = $con->query($query);
            while($row = $sql->fetch_assoc())
            {
                $sandboxToken = $row['token_sandbox'];
                $sandboxAuth = "Authorization: Bearer $sandboxToken";

                $ch = curl_init();
                curl_setopt_array($ch, array(
                    CURLOPT_URL => "https://api.sandbox.paypal.com/v2/checkout/orders",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST, true,
                    CURLOPT_POSTFIELDS => "{\n\t\"intent\" : \"CAPTURE\",\n\t\"purchase_units\" : [{\n\t\t\"amount\" : {\n\t\t\t\"currency_code\":\"USD\",\n\t\t\t\"value\":\"$orderPrice\"\n\t\t},\n\t\t\"payer\":{\n\t\t\t\"email_address\":\"$orderPayer\"\n\t\t}\n\t}]\n}",
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                        $sandboxAuth,
                        "Connection: keep-alive"
                    )
                ));

                $result = curl_exec($ch);
                $cleanResult = json_decode($result, true);

                $grabOrderID = $cleanResult['id'];

                $query2 = "INSERT INTO `payments` (`paypal_id`,`value`,`order`) VALUES ('$grabOrderID','$orderPrice','$orderId')";
                $sql2 = $con->query($query2);
                if($sql2)
                {
                    $redirect = $cleanResult['links']['1'];
                    $redirectURL = $redirect['href'];
                    return $redirectURL;
                }
            }
        }

        if($method == "live")
        {
            $query = "SELECT * FROM `paypal`";
            $sql = $con->query($query);
            while($row = $sql->fetch_assoc())
            {
                $sandboxToken = $row['token'];
                $sandboxAuth = "Authorization: Bearer $sandboxToken";

                $ch = curl_init();
                curl_setopt_array($ch, array(
                    CURLOPT_URL => "https://api.paypal.com/v2/checkout/orders",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST, true,
                    CURLOPT_POSTFIELDS => "{\n\t\"intent\" : \"CAPTURE\",\n\t\"purchase_units\" : [{\n\t\t\"amount\" : {\n\t\t\t\"currency_code\":\"USD\",\n\t\t\t\"value\":\"$orderPrice\"\n\t\t},\n\t\t\"payer\":{\n\t\t\t\"email_address\":\"$orderPayer\"\n\t\t}\n\t}]\n}",
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                        $sandboxAuth,
                        "Connection: keep-alive"
                    )
                ));

                $result = curl_exec($ch);
                $cleanResult = json_decode($result, true);

                $grabOrderID = $cleanResult['id'];

                $query2 = "INSERT INTO `payments` (`id`,`value`,`order`) VALUES ('$grabOrderID','$orderPrice','$orderId')";
                $sql2 = $con->query($query2);
                if($sql2)
                {
                    $redirect = $cleanResult['links']['1'];
                    $redirectURL = $redirect['href'];
                    return $redirectURL;
                }

            }
        }
    }

    function createPayment($orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
    }
}

?>