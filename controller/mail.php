<?php 

class Mail 
{

    public function subscribeUser($userMail,$orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        // Retrieve the email settings
        $query = "SELECT * FROM `settings` WHERE `name` = 'mail'";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $mailchimpKey = $row['key'];
            $mailchimpDC = $row['extra'];
        }

        // Make the request
        $auth = base64_encode("user:$mailchimpKey");

        $data = array (
            "apikey" => $mailchimpKey,
            "email_address" => $userMail,
            "status" => "subscribed",
            "merge_fields" => array(
                "ORDER" => $orderId
            )
        );

        $json_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://$mailchimpDC.api.mailchimp.com/3.0/lists/a3672dd76f/members/");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json",
                                                "Authorization: Basic $auth"));
        curl_setopt($ch, CURLOPT_USERAGENT, "PHP-MCAPI/2.0");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        return $httpcode;
    }

    public function sendConfirmation($firstName,$userMail)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        require "$docroot/sendgrip-php.php";
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@weare3dub.com", "World Wide Wrestling");
        $email->setSubject("Your order was successfully placed");
        $email->addTo("$userMail", "$userName");
        $email->addContent('text/html',
        '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <style type="text/css">
                body {width: 600px;margin: 0 auto;}
                table {border-collapse: collapse;}
                table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
                img {-ms-interpolation-mode: bicubic;}
            </style>
            <![endif]-->
                
            <style type="text/css">
                body, p, div {
                font-family: arial;
                font-size: 14px;
                }
                body {
                color: #000000;
                }
                body a {
                color: #1188E6;
                text-decoration: none;
                }
                p { margin: 0; padding: 0; }
                table.wrapper {
                width:100% !important;
                table-layout: fixed;
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: 100%;
                -moz-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                }
                img.max-width {
                max-width: 100% !important;
                }
                .column.of-2 {
                width: 50%;
                }
                .column.of-3 {
                width: 33.333%;
                }
                .column.of-4 {
                width: 25%;
                }
                @media screen and (max-width:480px) {
                .preheader .rightColumnContent,
                .footer .rightColumnContent {
                    text-align: left !important;
                }
                .preheader .rightColumnContent div,
                .preheader .rightColumnContent span,
                .footer .rightColumnContent div,
                .footer .rightColumnContent span {
                    text-align: left !important;
                }
                .preheader .rightColumnContent,
                .preheader .leftColumnContent {
                    font-size: 80% !important;
                    padding: 5px 0;
                }
                table.wrapper-mobile {
                    width: 100% !important;
                    table-layout: fixed;
                }
                img.max-width {
                    height: auto !important;
                    max-width: 480px !important;
                }
                a.bulletproof-button {
                    display: block !important;
                    width: auto !important;
                    font-size: 80%;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                }
                .columns {
                    width: 100% !important;
                }
                .column {
                    display: block !important;
                    width: 100% !important;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                    margin-left: 0 !important;
                    margin-right: 0 !important;
                }
                }
            </style>
            <!--user entered Head Start-->
            
                <!--End Head user entered-->
            </head>
            <body>
                <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
                    <div class="webkit">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
                            <tr>
                                <td valign="top" bgcolor="#ffffff" width="100%">
                                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="100%">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <!--[if mso]>
                                                            <center>
                                                            <table><tr><td width="600">
                                                            <![endif]-->
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                                                <tr>
                                                                    <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                                                        <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                                            <tr>
                                                                                <td role="module-content">
                                                                                    <p>Your order with World Wide Wrestling is on it the way!</p>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                
                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                    <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:50% !important;width:50%;height:auto !important;" src="https://shop.weare3dub.com/images/logo.png" alt="" width="300">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
            
                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                                    height="100%"
                                                                                    valign="top"
                                                                                    bgcolor="">
                                                                                    <div>Hi there '.$userName.' !</div>
                                                                    
                                                                                    <div>&nbsp;</div>
                                                                    
                                                                                    <div>Your order was successfully placed and the people here at World Wide Wrestling will be working hard to get it to you as soon as possible!</div>
                                                                                    <div>If you want to learn more about your order, you can find all the details on the orders page in your account</div>
                                            
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div data-role="module-unsubscribe" class="module unsubscribe-css__unsubscribe___2CDlR" role="module" data-type="unsubscribe" style="color:#444444;font-size:12px;line-height:20px;padding:16px 16px 16px 16px;text-align:center"><p style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px"><a class="Unsubscribe--unsubscribeLink" href="<%asm_group_unsubscribe_raw_url%>">Unsubscribe</a> - <a class="Unsubscribe--unsubscribePreferences" href="<%asm_preferences_raw_url%>">Unsubscribe Preferences</a></p></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        <!--[if mso]>
                                                        </td></tr></table>
                                                        </center>
                                                        <![endif]-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>
            </body>
        </html>  
        '
        );
    
        $sendgrid = new \SendGrid("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");
        try 
        {
            $response = $sendgrid->send($email);
            $statusCode = $response->statusCode();
            $responseBody = $response->body();
            $query = "INSERT INTO `apilog` (`vendor`,`code`,`message`) VALUES ('sendgrid','$statusCode','$responseBody')";
            $sql = $con->query($query);
        }
        catch (Excheption $e)
        {
            echo 'Caught exception: '.$e->getMessage() . "\n";
        }

    }

    public function sendShipment($userMail,$userName,$orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        require "$docroot/sendgrip-php.php";
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@weare3dub.com", "World Wide Wrestling");
        $email->setSubject("Your order is on it's way");
        $email->addTo("$userMail", "$userName");
        $email->addContent('text/html',
        '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <style type="text/css">
                body {width: 600px;margin: 0 auto;}
                table {border-collapse: collapse;}
                table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
                img {-ms-interpolation-mode: bicubic;}
            </style>
            <![endif]-->
                
            <style type="text/css">
                body, p, div {
                font-family: arial;
                font-size: 14px;
                }
                body {
                color: #000000;
                }
                body a {
                color: #1188E6;
                text-decoration: none;
                }
                p { margin: 0; padding: 0; }
                table.wrapper {
                width:100% !important;
                table-layout: fixed;
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: 100%;
                -moz-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                }
                img.max-width {
                max-width: 100% !important;
                }
                .column.of-2 {
                width: 50%;
                }
                .column.of-3 {
                width: 33.333%;
                }
                .column.of-4 {
                width: 25%;
                }
                @media screen and (max-width:480px) {
                .preheader .rightColumnContent,
                .footer .rightColumnContent {
                    text-align: left !important;
                }
                .preheader .rightColumnContent div,
                .preheader .rightColumnContent span,
                .footer .rightColumnContent div,
                .footer .rightColumnContent span {
                    text-align: left !important;
                }
                .preheader .rightColumnContent,
                .preheader .leftColumnContent {
                    font-size: 80% !important;
                    padding: 5px 0;
                }
                table.wrapper-mobile {
                    width: 100% !important;
                    table-layout: fixed;
                }
                img.max-width {
                    height: auto !important;
                    max-width: 480px !important;
                }
                a.bulletproof-button {
                    display: block !important;
                    width: auto !important;
                    font-size: 80%;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                }
                .columns {
                    width: 100% !important;
                }
                .column {
                    display: block !important;
                    width: 100% !important;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                    margin-left: 0 !important;
                    margin-right: 0 !important;
                }
                }
            </style>
            <!--user entered Head Start-->
            
                <!--End Head user entered-->
            </head>
            <body>
                <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
                    <div class="webkit">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
                            <tr>
                                <td valign="top" bgcolor="#ffffff" width="100%">
                                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="100%">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <!--[if mso]>
                                                            <center>
                                                            <table><tr><td width="600">
                                                            <![endif]-->
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                                                <tr>
                                                                    <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                                                        <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                                            <tr>
                                                                                <td role="module-content">
                                                                                    <p>Your order with World Wide Wrestling is on it the way!</p>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                
                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                    <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:50% !important;width:50%;height:auto !important;" src="https://shop.weare3dub.com/images/logo.png" alt="" width="300">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
            
                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                                    height="100%"
                                                                                    valign="top"
                                                                                    bgcolor="">
                                                                                    <div>Hi there '.$userName.' !</div>
                                                                    
                                                                                    <div>&nbsp;</div>
                                                                    
                                                                                    <div>The order you placed with ID '.$orderId.' at World Wide Wrestling is on it&#39;s way and will be available to you very soon!</div>
                                            
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div data-role="module-unsubscribe" class="module unsubscribe-css__unsubscribe___2CDlR" role="module" data-type="unsubscribe" style="color:#444444;font-size:12px;line-height:20px;padding:16px 16px 16px 16px;text-align:center"><p style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px"><a class="Unsubscribe--unsubscribeLink" href="<%asm_group_unsubscribe_raw_url%>">Unsubscribe</a> - <a class="Unsubscribe--unsubscribePreferences" href="<%asm_preferences_raw_url%>">Unsubscribe Preferences</a></p></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        <!--[if mso]>
                                                        </td></tr></table>
                                                        </center>
                                                        <![endif]-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>
            </body>
        </html>   
        '
        );
    
        $sendgrid = new \SendGrid("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");
        try 
        {
            $response = $sendgrid->send($email);
            $statusCode = $response->statusCode();
            $responseBody = $response->body();
            $query = "INSERT INTO `apilog` (`vendor`,`code`,`message`) VALUES ('sendgrid','$statusCode','$responseBody')";
            $sql = $con->query($query);
        }
        catch (Excheption $e)
        {
            echo 'Caught exception: '.$e->getMessage() . "\n";
        }
    }

    public function sendPaymentReminder($userMail,$userName,$orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        require "$docroot/sendgrip-php.php";
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@weare3dub.com", "World Wide Wrestling");
        $email->setSubject("We are still awaiting your payment");
        $email->addTo("$userMail", "$userName");
        $email->addContent('text/html',
        '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <style type="text/css">
                body {width: 600px;margin: 0 auto;}
                table {border-collapse: collapse;}
                table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
                img {-ms-interpolation-mode: bicubic;}
            </style>
            <![endif]-->
                
            <style type="text/css">
                body, p, div {
                font-family: arial;
                font-size: 14px;
                }
                body {
                color: #000000;
                }
                body a {
                color: #1188E6;
                text-decoration: none;
                }
                p { margin: 0; padding: 0; }
                table.wrapper {
                width:100% !important;
                table-layout: fixed;
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: 100%;
                -moz-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                }
                img.max-width {
                max-width: 100% !important;
                }
                .column.of-2 {
                width: 50%;
                }
                .column.of-3 {
                width: 33.333%;
                }
                .column.of-4 {
                width: 25%;
                }
                @media screen and (max-width:480px) {
                .preheader .rightColumnContent,
                .footer .rightColumnContent {
                    text-align: left !important;
                }
                .preheader .rightColumnContent div,
                .preheader .rightColumnContent span,
                .footer .rightColumnContent div,
                .footer .rightColumnContent span {
                    text-align: left !important;
                }
                .preheader .rightColumnContent,
                .preheader .leftColumnContent {
                    font-size: 80% !important;
                    padding: 5px 0;
                }
                table.wrapper-mobile {
                    width: 100% !important;
                    table-layout: fixed;
                }
                img.max-width {
                    height: auto !important;
                    max-width: 480px !important;
                }
                a.bulletproof-button {
                    display: block !important;
                    width: auto !important;
                    font-size: 80%;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                }
                .columns {
                    width: 100% !important;
                }
                .column {
                    display: block !important;
                    width: 100% !important;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                    margin-left: 0 !important;
                    margin-right: 0 !important;
                }
                }
            </style>
            <!--user entered Head Start-->
            
                <!--End Head user entered-->
            </head>
            <body>
                <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
                    <div class="webkit">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
                            <tr>
                                <td valign="top" bgcolor="#ffffff" width="100%">
                                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="100%">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <!--[if mso]>
                                                            <center>
                                                            <table><tr><td width="600">
                                                            <![endif]-->
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                                                <tr>
                                                                    <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                                                        <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                                            <tr>
                                                                                <td role="module-content">
                                                                                    <p>We are awaiting your payment</p>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                
                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                    <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:50% !important;width:50%;height:auto !important;" src="https://shop.weare3dub.com/images/logo.png" alt="" width="300">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
            
                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                                    height="100%"
                                                                                    valign="top"
                                                                                    bgcolor="">
                                                                                    <div>Hi there '.$userName.' !</div>
                                                                    
                                                                                    <div>&nbsp;</div>
                                                                    
                                                                                    <div>We see you have tried to place an order. Unfortunately, for some reason the payment did not process correctly.</div>
                                                                                    <div>To complete your order, click the button below.</div>

                                                                                    <table border="0" cellPadding="0" cellSpacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td align="center" bgcolor="" class="outer-td" style="padding:0px 0px 0px 0px">
                                                                                                    <table border="0" cellPadding="0" cellSpacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="center" bgcolor="#000" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit">
                                                                                                                    <a href="https://shop.weare3dub.com/shop/payment-reminder?id='.$orderId.'" style="background-color:#000;border:1px solid #333333;border-color:#000;border-radius:0px;border-width:1px;color:#ffffff;display:inline-block;font-family:arial,helvetica,sans-serif;font-size:16px;font-weight:normal;letter-spacing:0px;line-height:16px;padding:12px 18px 12px 18px;text-align:center;text-decoration:none" target="_blank">
                                                                                                                        Complete the order
                                                                                                                    </a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div data-role="module-unsubscribe" class="module unsubscribe-css__unsubscribe___2CDlR" role="module" data-type="unsubscribe" style="color:#444444;font-size:12px;line-height:20px;padding:16px 16px 16px 16px;text-align:center"><p style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px"><a class="Unsubscribe--unsubscribeLink" href="<%asm_group_unsubscribe_raw_url%>">Unsubscribe</a> - <a class="Unsubscribe--unsubscribePreferences" href="<%asm_preferences_raw_url%>">Unsubscribe Preferences</a></p></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        <!--[if mso]>
                                                        </td></tr></table>
                                                        </center>
                                                        <![endif]-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>
            </body>
        </html>  
        '
        );
    
        $sendgrid = new \SendGrid("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");
        try 
        {
            $response = $sendgrid->send($email);
            $statusCode = $response->statusCode();
            $responseBody = $response->body();
            $query = "INSERT INTO `apilog` (`vendor`,`code`,`message`) VALUES ('sendgrid','$statusCode','$responseBody')";
            $sql = $con->query($query);
        }
        catch (Excheption $e)
        {
            echo 'Caught exception: '.$e->getMessage() . "\n";
        }
    }

    public function sendNotification($orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        require "$docroot/sendgrip-php.php";
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@weare3dub.com", "World Wide Wrestling");
        $email->setSubject("Someone has placed an order");
        $email->addTo("jeroenvankuijk1@outlook.com", "Jeroen van Kuijk");
        $email->addContent('text/html',
        '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <style type="text/css">
                body {width: 600px;margin: 0 auto;}
                table {border-collapse: collapse;}
                table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
                img {-ms-interpolation-mode: bicubic;}
            </style>
            <![endif]-->
                
            <style type="text/css">
                body, p, div {
                font-family: arial;
                font-size: 14px;
                }
                body {
                color: #000000;
                }
                body a {
                color: #1188E6;
                text-decoration: none;
                }
                p { margin: 0; padding: 0; }
                table.wrapper {
                width:100% !important;
                table-layout: fixed;
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: 100%;
                -moz-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                }
                img.max-width {
                max-width: 100% !important;
                }
                .column.of-2 {
                width: 50%;
                }
                .column.of-3 {
                width: 33.333%;
                }
                .column.of-4 {
                width: 25%;
                }
                @media screen and (max-width:480px) {
                .preheader .rightColumnContent,
                .footer .rightColumnContent {
                    text-align: left !important;
                }
                .preheader .rightColumnContent div,
                .preheader .rightColumnContent span,
                .footer .rightColumnContent div,
                .footer .rightColumnContent span {
                    text-align: left !important;
                }
                .preheader .rightColumnContent,
                .preheader .leftColumnContent {
                    font-size: 80% !important;
                    padding: 5px 0;
                }
                table.wrapper-mobile {
                    width: 100% !important;
                    table-layout: fixed;
                }
                img.max-width {
                    height: auto !important;
                    max-width: 480px !important;
                }
                a.bulletproof-button {
                    display: block !important;
                    width: auto !important;
                    font-size: 80%;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                }
                .columns {
                    width: 100% !important;
                }
                .column {
                    display: block !important;
                    width: 100% !important;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                    margin-left: 0 !important;
                    margin-right: 0 !important;
                }
                }
            </style>
            <!--user entered Head Start-->
            
                <!--End Head user entered-->
            </head>
            <body>
                <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
                    <div class="webkit">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
                            <tr>
                                <td valign="top" bgcolor="#ffffff" width="100%">
                                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="100%">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <!--[if mso]>
                                                            <center>
                                                            <table><tr><td width="600">
                                                            <![endif]-->
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                                                <tr>
                                                                    <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                                                        <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                                            <tr>
                                                                                <td role="module-content">
                                                                                    <p>We are awaiting your payment</p>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                
                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                    <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:50% !important;width:50%;height:auto !important;" src="https://shop.weare3dub.com/images/logo.png" alt="" width="300">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
            
                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                                    height="100%"
                                                                                    valign="top"
                                                                                    bgcolor="">
                                                                                    <div>Hi there Jeroen!</div>
                                                                    
                                                                                    <div>&nbsp;</div>
                                                                    
                                                                                    <div>Someone has placed an order on the 3DUB Webshop.</div>
                                                                                    <div>To view the order, click the button below.</div>

                                                                                    <table border="0" cellPadding="0" cellSpacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td align="center" bgcolor="" class="outer-td" style="padding:0px 0px 0px 0px">
                                                                                                    <table border="0" cellPadding="0" cellSpacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="center" bgcolor="#000" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit">
                                                                                                                    <a href="https://shop.weare3dub.com/admin/ordernotification?id='.$orderId.'" style="background-color:#000;border:1px solid #333333;border-color:#000;border-radius:0px;border-width:1px;color:#ffffff;display:inline-block;font-family:arial,helvetica,sans-serif;font-size:16px;font-weight:normal;letter-spacing:0px;line-height:16px;padding:12px 18px 12px 18px;text-align:center;text-decoration:none" target="_blank">
                                                                                                                        View the order
                                                                                                                    </a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div data-role="module-unsubscribe" class="module unsubscribe-css__unsubscribe___2CDlR" role="module" data-type="unsubscribe" style="color:#444444;font-size:12px;line-height:20px;padding:16px 16px 16px 16px;text-align:center"><p style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px"><a class="Unsubscribe--unsubscribeLink" href="<%asm_group_unsubscribe_raw_url%>">Unsubscribe</a> - <a class="Unsubscribe--unsubscribePreferences" href="<%asm_preferences_raw_url%>">Unsubscribe Preferences</a></p></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        <!--[if mso]>
                                                        </td></tr></table>
                                                        </center>
                                                        <![endif]-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>
            </body>
        </html>  
        '
        );
    
        $sendgrid = new \SendGrid("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");
        try 
        {
            $response = $sendgrid->send($email);
            $statusCode = $response->statusCode();
            $responseBody = $response->body();
            $query = "INSERT INTO `apilog` (`vendor`,`code`,`message`) VALUES ('sendgrid','$statusCode','$responseBody')";
            $sql = $con->query($query);
        }
        catch (Excheption $e)
        {
            echo 'Caught exception: '.$e->getMessage() . "\n";
        }
    }

    public function sendProduction($orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        require "$docroot/sendgrip-php.php";
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@weare3dub.com", "World Wide Wrestling");
        $email->setSubject("We want to place an order");
        $email->addTo("ladykcreations2@icloud.com", "Chiquita");
        $email->addContent('text/html',
        '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <style type="text/css">
                body {width: 600px;margin: 0 auto;}
                table {border-collapse: collapse;}
                table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
                img {-ms-interpolation-mode: bicubic;}
            </style>
            <![endif]-->
                
            <style type="text/css">
                body, p, div {
                font-family: arial;
                font-size: 14px;
                }
                body {
                color: #000000;
                }
                body a {
                color: #1188E6;
                text-decoration: none;
                }
                p { margin: 0; padding: 0; }
                table.wrapper {
                width:100% !important;
                table-layout: fixed;
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: 100%;
                -moz-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                }
                img.max-width {
                max-width: 100% !important;
                }
                .column.of-2 {
                width: 50%;
                }
                .column.of-3 {
                width: 33.333%;
                }
                .column.of-4 {
                width: 25%;
                }
                @media screen and (max-width:480px) {
                .preheader .rightColumnContent,
                .footer .rightColumnContent {
                    text-align: left !important;
                }
                .preheader .rightColumnContent div,
                .preheader .rightColumnContent span,
                .footer .rightColumnContent div,
                .footer .rightColumnContent span {
                    text-align: left !important;
                }
                .preheader .rightColumnContent,
                .preheader .leftColumnContent {
                    font-size: 80% !important;
                    padding: 5px 0;
                }
                table.wrapper-mobile {
                    width: 100% !important;
                    table-layout: fixed;
                }
                img.max-width {
                    height: auto !important;
                    max-width: 480px !important;
                }
                a.bulletproof-button {
                    display: block !important;
                    width: auto !important;
                    font-size: 80%;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                }
                .columns {
                    width: 100% !important;
                }
                .column {
                    display: block !important;
                    width: 100% !important;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                    margin-left: 0 !important;
                    margin-right: 0 !important;
                }
                }
            </style>
            <!--user entered Head Start-->
            
                <!--End Head user entered-->
            </head>
            <body>
                <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
                    <div class="webkit">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
                            <tr>
                                <td valign="top" bgcolor="#ffffff" width="100%">
                                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="100%">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <!--[if mso]>
                                                            <center>
                                                            <table><tr><td width="600">
                                                            <![endif]-->
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                                                <tr>
                                                                    <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                                                        <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                                            <tr>
                                                                                <td role="module-content">
                                                                                    <p></p>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                
                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                    <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:50% !important;width:50%;height:auto !important;" src="https://shop.weare3dub.com/images/logo.png" alt="" width="300">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
            
                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                                    height="100%"
                                                                                    valign="top"
                                                                                    bgcolor="">
                                                                                    <div>Hi there Chiquita!</div>
                                                                    
                                                                                    <div>&nbsp;</div>
                                                                    
                                                                                    <div>We would like to place an order for a black t-shirt.</div>
                                                                                    <div>You can find all of the details by clicking the button below.</div>
                                                                                    <div>Could you please send a PayPal invoice to info@weare3dub.com?</div>

                                                                                    <table border="0" cellPadding="0" cellSpacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td align="center" bgcolor="" class="outer-td" style="padding:0px 0px 0px 0px">
                                                                                                    <table border="0" cellPadding="0" cellSpacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="center" bgcolor="#000" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit">
                                                                                                                    <a href="https://shop.weare3dub.com/shop/view-order?id='.$orderId.'" style="background-color:#000;border:1px solid #333333;border-color:#000;border-radius:0px;border-width:1px;color:#ffffff;display:inline-block;font-family:arial,helvetica,sans-serif;font-size:16px;font-weight:normal;letter-spacing:0px;line-height:16px;padding:12px 18px 12px 18px;text-align:center;text-decoration:none" target="_blank">
                                                                                                                        View the order
                                                                                                                    </a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div data-role="module-unsubscribe" class="module unsubscribe-css__unsubscribe___2CDlR" role="module" data-type="unsubscribe" style="color:#444444;font-size:12px;line-height:20px;padding:16px 16px 16px 16px;text-align:center"><p style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px"><a class="Unsubscribe--unsubscribeLink" href="<%asm_group_unsubscribe_raw_url%>">Unsubscribe</a> - <a class="Unsubscribe--unsubscribePreferences" href="<%asm_preferences_raw_url%>">Unsubscribe Preferences</a></p></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        <!--[if mso]>
                                                        </td></tr></table>
                                                        </center>
                                                        <![endif]-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>
            </body>
        </html>  
        '
        );
    
        $sendgrid = new \SendGrid("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");
        try 
        {
            $response = $sendgrid->send($email);
            $statusCode = $response->statusCode();
            $responseBody = $response->body();
            $query = "INSERT INTO `apilog` (`vendor`,`code`,`message`) VALUES ('sendgrid','$statusCode','$responseBody')";
            $sql = $con->query($query);
        }
        catch (Excheption $e)
        {
            echo 'Caught exception: '.$e->getMessage() . "\n";
        }
    }

    public function claimCoupon($couponId,$userMail,$userName)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        require "$docroot/sendgrip-php.php";
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@weare3dub.com", "World Wide Wrestling");
        $email->setSubject("You have claimed a new coupon");
        $email->addTo("$userMail", "$userName");
        $email->addContent('text/html',
        '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <!--[if (gte mso 9)|(IE)]>
            <style type="text/css">
                body {width: 600px;margin: 0 auto;}
                table {border-collapse: collapse;}
                table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
                img {-ms-interpolation-mode: bicubic;}
            </style>
            <![endif]-->
                
            <style type="text/css">
                body, p, div {
                font-family: arial;
                font-size: 14px;
                }
                body {
                color: #000000;
                }
                body a {
                color: #1188E6;
                text-decoration: none;
                }
                p { margin: 0; padding: 0; }
                table.wrapper {
                width:100% !important;
                table-layout: fixed;
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: 100%;
                -moz-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                }
                img.max-width {
                max-width: 100% !important;
                }
                .column.of-2 {
                width: 50%;
                }
                .column.of-3 {
                width: 33.333%;
                }
                .column.of-4 {
                width: 25%;
                }
                @media screen and (max-width:480px) {
                .preheader .rightColumnContent,
                .footer .rightColumnContent {
                    text-align: left !important;
                }
                .preheader .rightColumnContent div,
                .preheader .rightColumnContent span,
                .footer .rightColumnContent div,
                .footer .rightColumnContent span {
                    text-align: left !important;
                }
                .preheader .rightColumnContent,
                .preheader .leftColumnContent {
                    font-size: 80% !important;
                    padding: 5px 0;
                }
                table.wrapper-mobile {
                    width: 100% !important;
                    table-layout: fixed;
                }
                img.max-width {
                    height: auto !important;
                    max-width: 480px !important;
                }
                a.bulletproof-button {
                    display: block !important;
                    width: auto !important;
                    font-size: 80%;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                }
                .columns {
                    width: 100% !important;
                }
                .column {
                    display: block !important;
                    width: 100% !important;
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                    margin-left: 0 !important;
                    margin-right: 0 !important;
                }
                }
            </style>
            <!--user entered Head Start-->
            
                <!--End Head user entered-->
            </head>
            <body>
                <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
                    <div class="webkit">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
                            <tr>
                                <td valign="top" bgcolor="#ffffff" width="100%">
                                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="100%">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <!--[if mso]>
                                                            <center>
                                                            <table><tr><td width="600">
                                                            <![endif]-->
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                                                <tr>
                                                                    <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                                                        <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                                            <tr>
                                                                                <td role="module-content">
                                                                                    <p></p>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                
                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                    <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:50% !important;width:50%;height:auto !important;" src="https://shop.weare3dub.com/images/logo.png" alt="" width="300">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
            
                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                            <tr>
                                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                                    height="100%"
                                                                                    valign="top"
                                                                                    bgcolor="">
                                                                                    <div>Hi there Chiquita!</div>
                                                                    
                                                                                    <div>&nbsp;</div>
                                                                    
                                                                                    <div>You have successfully claimed a new coupon. Use code '.$couponId.' on your next order for a 10% discount!</div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div data-role="module-unsubscribe" class="module unsubscribe-css__unsubscribe___2CDlR" role="module" data-type="unsubscribe" style="color:#444444;font-size:12px;line-height:20px;padding:16px 16px 16px 16px;text-align:center"><p style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px"><a class="Unsubscribe--unsubscribeLink" href="<%asm_group_unsubscribe_raw_url%>">Unsubscribe</a> - <a class="Unsubscribe--unsubscribePreferences" href="<%asm_preferences_raw_url%>">Unsubscribe Preferences</a></p></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        <!--[if mso]>
                                                        </td></tr></table>
                                                        </center>
                                                        <![endif]-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>
            </body>
        </html>  
        '
        );
    
        $sendgrid = new \SendGrid("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");
        try 
        {
            $response = $sendgrid->send($email);
            $statusCode = $response->statusCode();
            $responseBody = $response->body();
            $query = "INSERT INTO `apilog` (`vendor`,`code`,`message`) VALUES ('sendgrid','$statusCode','$responseBody')";
            $sql = $con->query($query);
        }
        catch (Excheption $e)
        {
            echo 'Caught exception: '.$e->getMessage() . "\n";
        }
    }
}

?>