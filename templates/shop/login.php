<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h3>Do you want to login with your account?</h3>
        <form method="post" action="/action/loginorder">
            <input type="text" class="w3-input w3-margin-bottom" name="username" placeholder="Username" required>
            <input type="password" class="w3-input w3-margin-bottom" placeholder="Password" name="pass" required>
            <input type="hidden" name="product" value="<?php echo $productId;?>">
            <input type="submit" class="w3-button w3-yellow w3-hover-black">
        </form>
        <a href="/shop/order-guest">No, I want to order as a guest</a>
    </div>
</div>