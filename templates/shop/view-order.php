<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h3>The order</h3>
        <?php 
            $productName = $getOrder['product'];
            $query = "SELECT * FROM `products` WHERE `name` = '$productName'";
            $sql = $con->query($query);
            while($row = $sql->fetch_assoc())
            {
                $productImage = $row['image'];
            }
        ?>
        <img src="/images/<?php echo $productImage; ?>" class="w3-image">
        <br/>
        <p>
            <em>Right click the image to save it</em>
        </p>

        <div class="w3-half">
            <h3>Order details</h3>
            <table class="w3-table">
                <tr>
                    <td><strong>Product</strong></td>
                    <td>T-shirt</td>
                </tr>
                <tr>
                    <td><strong>Color</strong></td>
                    <td>Black</td>
                </tr>
                <tr>
                    <td><strong>Size</strong></td>
                    <td><?php echo $getOrder['size']; ?></td>
                </tr>
                <tr>
                    <td><strong>Product name</strong></td>
                    <td><?php echo $getOrder['product']; ?></td>
                </tr>
            </table>
        </div>

        <div class="w3-half">
            <h3>Shipping details</h3>
            <table class="w3-table">
                <tr>
                    <td><strong>Client name</strong></td>
                    <td><?php echo $getOrder['name']; ?></td>
                </tr>
                <tr>
                    <td><strong>Client address</strong></td>
                    <td><?php echo $getOrder['address']; ?></td>
                </tr>
                <tr>
                    <td><strong>Client city</strong></td>
                    <td><?php echo $getOrder['city']; ?></td>
                </tr>
                <tr>
                    <td><strong>Client postal code</strong></td>
                    <td><?php echo $getOrder['zip']; ?></td>
                </tr>
                <tr>
                    <td><strong>Client state</strong></td>
                    <td><?php echo $getOrder['state']; ?></td>
                </tr>
                <tr>
                    <td><strong>Client country</strong></td>
                    <td><?php echo $getOrder['country']; ?></td>
                </tr>
            </table>            
        </div>

    </div>
<?php if($getOrder['status'] == '2' ){?>
    <div class="w3-row">
        <form method="post">
            <p>Please confirm that you have received the order and that it gets shipped to the client</p>
            <input type="submit" class="w3-button w3-margin-bottom w3-margin-top w3-yellow w3-hover-black" value="Confirm" name="confirm">
        </form>
    </div>
<?php } ?>
</div>