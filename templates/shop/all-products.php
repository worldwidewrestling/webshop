<div class="w3-display-container w3-container w3-center">
    <?php 
        $query = "SELECT * FROM `products` WHERE `active` = '1'";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $productName = $row['name'];
            $productDesc = $row['description'];
            $productImg = $row['image'];
            $productPrice = $row['price'];
            $productId = $row['id'];

            echo '
            <div class="w3-row">
                <div class="w3-half">
                    '.$productName.'
                    <br/>
                    <i>'.$productDesc.'</i>
                    <br/>
                    <strong>'.$productPrice.'</strong>
                    <br/>
                    <a href="/shop/order?id='.$productId.'"><button class="w3-button w3-black w3-hover-yellow">Order</button></a>
                </div>
                <div class="w3-half">
                    <img class="w3-image" src="/images/'.$productImg.'"/>
                </div>
            </div>
            <hr/>
            ';

        }
    ?>
</div>