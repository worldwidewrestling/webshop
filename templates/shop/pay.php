<div class="w3-display-container w3-container w3-center">
    <h3>Payment</h3>
    <p>Your order will be completed upon payment.</p>

    <?php 
        $query = "SELECT * FROM `paypal`";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $method = $row['method'];
            $sandbox = $row['sandbox'];
            $live = $row['live'];

            if($method == "sandbox")
            {
                ?>
                
                <script
                    src="https://www.paypal.com/sdk/js?client-id=<?php echo $sandbox; ?>&currency=USD">
                </script>

                <div id="paypal-button-container">
                    <script>
                    paypal.Buttons({
                        createOrder: function(data, actions) {
                        return actions.order.create({
                            purchase_units: [{
                            amount: {
                                value: "<?php echo $setPrice; ?>"
                            }
                            }]
                        });
                        },
                        onApprove: function(data, actions) {
                        return actions.order.capture().then(function(details) {
                            window.location.replace("/shop/thanks");
                            // Call your server to save the transaction
                            return fetch('/paypal-transaction-complete', {
                            method: 'post',
                            headers: {
                                'content-type': 'application/json'
                            },
                            body: JSON.stringify({
                                orderID: data.orderID
                            })
                            });
                        });
                        }
                    }).render('#paypal-button-container');
                    </script>
                </div>

            <?php }elseif($method == "live"){ ?>

                <script
                    src="https://www.paypal.com/sdk/js?client-id=<?php echo $live; ?>&currency=USD">
                </script>

                <div id="paypal-button-container">
                    <script>
                    paypal.Buttons({
                        createOrder: function(data, actions) {
                        return actions.order.create({
                            purchase_units: [{
                            amount: {
                                value: '<?php echo $setPrice; ?>'
                            }
                            }]
                        });
                        },
                        onApprove: function(data, actions) {
                        return actions.order.capture().then(function(details) {
                            window.location.replace("/shop/thanks");
                            // Call your server to save the transaction
                            return fetch('/paypal-transaction-complete', {
                            method: 'post',
                            headers: {
                                'content-type': 'application/json'
                            },
                            body: JSON.stringify({
                                orderID: data.orderID
                            })
                            });
                        });
                        }
                    }).render('#paypal-button-container');
                    </script>
                </div>

            <?php } ?>

        <?php } ?>

</div>

