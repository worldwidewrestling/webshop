<div class="w3-display-container w3-container w3-center">

    <h1>This is our main event!</h1>
    <p>In our main event, you will find some exclusive items every once in a while!</p>
    <div class="w3-row">
        <?php 
            $query = "SELECT * FROM `products` WHERE `spotlight` = '1'";
            $sql = $con->query($query);
            while($row = $sql->fetch_assoc())
            {
                $spotlightName = $row['name'];
                $spotlightDescription = $row['description'];
                $spotlightPrice = $row['price'];
                $spotlightImage = $row['image'];
                $spotlightId = $row['id'];

                echo '
                    <div class="w3-half">
                        <h3>'.$spotlightName.'</h3>
                        <p>'.$spotlightDescription.'</p>
                        <img src="/images/'.$spotlightImage.'" width="50%">
                        <br/><br/>
                        <a href="/shop/order?id='.$spotlightId.'"><button class="w3-button w3-black w3-hover-yellow">Order</button></a>
                    </div>
                
                
                ';

            }
        ?>

</div>