<div class="w3-display-container w3-container w3-center">
    <h1>Your Profile</h1>

    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Profile</button>
        <button class="tablinks" onclick="openCity(event, 'Paris')">Order History</button>
        <button class="tablinks" onclick="openCity(event, 'Tokyo')">Loyalty</button>
        <!-- <button class="tablinks" onclick="openCity(event, 'Amsterdam')">Fanclub</button> -->
    </div>

    <div id="London" class="tabcontent">
    <?php 
        $query = "SELECT * FROM `users` WHERE `id` = '$user_id'";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $userName = $row['name'];
            $userMail = $row['mail'];
            $userAddress = $row['address'];
            $userZIP = $row['zip'];
            $userCity = $row['city'];
            $userState = $row['state'];
            $userCountry = $row['country'];
            $userPoints = $row['points'];

            if($userName == NULL)
            {
                $userName = "Your full name";
            }

            if($userMail == NULL)
            {
                $userMail = "Your email address";
            }

            if($userAddress == NULL)
            {
                $userAddress = "Your address";
            }

            if($userZIP == NULL)
            {
                $userZIP = "Your postal code";
            }

            if($userCity == NULL)
            {
                $userCity = "Your city";
            }

            if($userState == NULL)
            {
                $userState = "Your state";
            }

            if($userCountry == NULL)
            {
                $userCountry = "Your country";
            }

        }
    ?>
        <p>Please fill in the missing fields so we can send all of your orders the right way.</p>
        <form action="/action/updateprofile" method="post">
            <input type="hidden" name="user" value="<?php echo $user_id ?>">
            <input type="text" class="w3-input w3-margin-bottom" name="name" value="<?php echo $userName; ?>">
            <input type="email" class="w3-input w3-margin-bottom" name="mail" value="<?php echo $userMail; ?>">
            <input type="text" class="w3-input w3-margin-bottom" name="address" value="<?php echo $userAddress; ?>">
            <input type="text" class="w3-input w3-margin-bottom" name="zip" value="<?php echo $userZIP; ?>">
            <input type="text" class="w3-input w3-margin-bottom" name="city" value="<?php echo $userCity; ?>">
            <input type="text" class="w3-input w3-margin-bottom" name="state" value="<?php echo $userState; ?>">
            <input type="text" class="w3-input w3-margin-bottom" name="country" value="<?php echo $userCountry; ?>">
            <input type="submit" class="w3-button w3-yellow w3-margin-bottom" name="submit" value="Submit">
        </form>
    </div>

    <div id="Paris" class="tabcontent">
        <?php 
            $query = "SELECT * FROM `orders` WHERE `client` = '$user_id'";
            $sql = $con->query($query);
            $count = mysqli_num_rows($sql);
            if($count == '0')
            {
                echo "<p>You have not placed an order yet</p>";
            }
            else 
            { ?>
            <table class="w3-table">
                <thead>
                    <th>Order no.</th>
                    <th>Order product</th>
                    <th>Order status</th>
                </thead>
                <tbody>
                    <?php 
                        $query2 = "SELECT * FROM `orders` WHERE `client` = '$user_id'";
                        $sql2 = $con->query($query2);
                        while($row = $sql2->fetch_assoc())
                        {
                            $orderId = $row['id'];
                            $orderProduct = $row['product'];
                            $orderStatus = $row['status'];

                            if($orderStatus == '1')
                            {
                                $orderStatus = "Awaiting payment.";
                            }
                            elseif($orderStatus == '2')
                            {
                                $orderStatus = "Waiting for fulfilment";
                            }
                            elseif($orderStatus == '3')
                            {
                                $orderStatus = "Shipped";
                            }

                            echo "
                            <tr>
                                <td>$orderId</td>
                                <td>$orderProduct</td>
                                <td>$orderStatus</td>
                            ";

                        }
                    ?>
                </tbody>
            </table>
            <?php } ?>
    </div>

    <div id="Tokyo" class="tabcontent">
        <p>
            With each order, you will gain 5 new loyalty points.
            <br/>
            After saving 100 points, you can exchange them for a loyalty coupon. This coupon gives you the right to 10% off of your next order. Each coupon can only be used once!
        </p>
        <p>
            You currently have : <strong><?php echo $userPoints;?> points </strong>
        </p>

        <hr/>

        <?php 
            if($userPoints >= '100')
            {
                echo '<p>Congratulations, you can claim a coupon. Press the button below!</p>';
                echo '<a href="/action/claimcoupon"><button class="w3-button w3-yellow w3-hover-black">Claim coupon</button></a>';
                echo '<hr/>';
            }
        ?>

        <!-- Check whether the user has active coupons -->
        <h3>My coupons</h3>
        <?php 
            $query3 = "SELECT * FROM `coupons` WHERE `user_id` = '$user_id' AND `active` = '1'";
            $sql3 = $con->query($query3);
            $totalCoupons = mysqli_num_rows($sql3);

            if($totalCoupons == '0')
            {
                echo '<p>You currently do not have any coupons. Please claim some</p>';
            }
            else 
            {
            ?>

            <table class="w3-table">
                <thead>
                    <th>Coupon ID</th>
                </thead>
                <tbody>
                    <?php 
                        while($row2 = $sql3->fetch_assoc())
                        {
                            $couponId = $row2['coupon_id'];

                            echo "
                            <tr>
                                <td>$couponId</td>
                            ";

                        }
                    ?>
                </tbody>
            </table>
            <?php } ?>


    </div>

    <!--<div id="Amsterdam" class="tabcontent">

        <?php 
            $query = "SELECT * FROM `users` WHERE `id` = '$user_id'";
            $sql = $con->query($query);
            while($row = $sql->fetch_assoc())
            {
                $userSubscription = $row['subscription'];
                $userSubscriptionEnd = $row['subscription_till'];
            }
        ?>

        <p>
            The World Wide Wrestling subscription allows you to enjoy a lot of special benefits and helps you show off your loyalty to your favorite e-fed in the world.
        </p>

        <?php 
            if($userSubscription == '0')
            {
        ?>

        <p>
            You are currently not a member of the 3DUB Fanclub.
            <br/>
            If you wish to become a member, select one of the options below.
        </p>

        <?php 
            }
        ?>

    </div>-->

</div>