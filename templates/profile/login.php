<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <div class="w3-half">
            <h3>Returning user?</h3>
            <form method="post">
                <input type="text" class="w3-input w3-margin-bottom" name="username" placeholder="Username" required>
                <input type="password" class="w3-input w3-margin-bottom" placeholder="Password" name="pass" required>
                <input type="submit" name="login" class="w3-button w3-yellow w3-hover-black">
            </form>
        </div>
        <div class="w3-half">
            <h3>New user?</h3>
            <form method="post">
                <input type="text" class="w3-input w3-margin-bottom" name="username" placeholder="Username" required>
                <input type="password" class="w3-input w3-margin-bottom" name="pass" placeholder="Password" required>
                <input type="submit" name="register" class="w3-button w3-yellow w3-hover-black">
            </form>
        </div>
    </div>
</div>