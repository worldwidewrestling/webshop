<div class="w3-display-container w3-container w3-center">
    <h1>Change the world</h1>
    <p>
        Over the past few years, we have shown that 3DUB is one of the biggest, most exciting and engaging e-fed in the business. 
        If you wish to support World Wide Wrestling, but don't want to buy our merchandise, we also offer the option to make a custom donation by using the button below.
    </p>
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="RWXR2ZQGJDWM6" />
        <input type="image" src="https://www.paypalobjects.com/en_US/NL/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
        <img alt="" border="0" src="https://www.paypal.com/en_NL/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>