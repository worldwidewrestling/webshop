<?php
$docroot = $_SERVER['DOCUMENT_ROOT'];
$page = "Home";
include("$docroot/templates/header.php");
?>

<!-- Image header -->
<div class="w3-display-container w3-container">
    <img src="https://ak7.picdn.net/shutterstock/videos/2034937/thumb/1.jpg" alt="Jeans" style="width:100%;height:200px">
    <div class="w3-display-topleft w3-text-white" style="padding:24px 48px">
        <h1>Official 3DUB Merch</h1>
        <p><a href="/shop/collection" class="w3-button w3-yellow w3-padding-large w3-large">SHOP NOW</a></p>
    </div>
</div>

<div class="w3-display-container w3-container w3-center">
    <hr/>
</div>

<?php 
include("$docroot/templates/profile/product.php");
?>  

<div class="w3-display-container w3-container w3-center">
    <hr/>
</div>

<?php 
include("$docroot/templates/footer.php");
?>  