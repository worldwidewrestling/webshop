<?php
$docroot = $_SERVER['DOCUMENT_ROOT'];
$page = "Home";
$error = $_GET['a'];
include("$docroot/templates/header.php");
?>

<!-- Image header -->
<div class="w3-display-container w3-container">
    <img src="https://ak7.picdn.net/shutterstock/videos/2034937/thumb/1.jpg" alt="Jeans" style="width:100%;height:200px">
    <div class="w3-display-topleft w3-text-white" style="padding:24px 48px">
        <h1>Whoops</h1>
        <?php 
        if($error = 'invalidpass')
        {
            echo "<h3>You have entered an invalid password</h3>";
        }
        elseif($error = 'creationfailed')
        {
            echo "<h3>Account creation failed. Please try again</h3>";
        }
        ?>
    </div>
</div>

<div class="w3-display-container w3-container w3-center">
    <hr/>
</div>

<?php 
include("$docroot/templates/footer.php");
?>  