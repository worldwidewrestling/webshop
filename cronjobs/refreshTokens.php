<?php 

$con = mysqli_connect("localhost","3dub_staging","Websh0pStaging19!","vankuijk_shop-staging");

$query = "SELECT * FROM `paypal`";
$sql = $con->query($query);
while($row = $sql->fetch_assoc())
{
    $sandboxClient = $row['sandbox'];
    $sandboxSecret = $row['sandbox_secret'];
    $liveClient = $row['live'];
    $liveSecret = $row['live_secret'];

    // Sandbox token
    $sandboxAuth = "$sandboxClient:$sandboxSecret";
    $post_fields = "grant_type=client_credentials";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $sandboxAuth);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"
                                        ));
    curl_setopt($ch, CURLOPT_USERAGENT, "PHP-MCAPI/2.0");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    $resultSandbox = curl_exec($ch);
    curl_close($ch);
    $arraySandbox = json_decode($resultSandbox, true);
    $sandboxToken = $arraySandbox['access_token'];

    // Live token
    $liveAuth = "$liveClient:$liveSecret";

    $ch2 = curl_init();
    curl_setopt($ch2, CURLOPT_URL, "https://api.paypal.com/v1/oauth2/token");
    curl_setopt($ch2, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch2, CURLOPT_USERPWD, $liveAuth);
    curl_setopt($ch2, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"
                                        ));
    curl_setopt($ch2, CURLOPT_USERAGENT, "PHP-MCAPI/2.0");
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch2, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch2, CURLOPT_POST, true);
    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch2, CURLOPT_POSTFIELDS, $post_fields);
    $resultLive = curl_exec($ch2);
    curl_close($ch2);
    $arrayLive = json_decode($resultLive, true);
    $liveToken = $arrayLive['access_token'];

    $query2 = "UPDATE `paypal` SET `token` = '$liveToken',`token_sandbox` = '$sandboxToken'";
    $sql2 = $con->query($query2);

    exit();

}



?>