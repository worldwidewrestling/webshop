<?php
$docroot = $_SERVER['DOCUMENT_ROOT'];
$page = "Home";
include("$docroot/templates/header.php");
include("$docroot/controller/AccountController.php");
$account = new Account;

if(isset($_POST['register']))
{
    $userName = $_POST['username'];
    $userPass = $_POST['pass'];
    $registerAccount = $account->registerAccount($userName,$userPass);
    if($registerAccount == "OK")
    {
        header('location: success');
    }
    else
    {
        header('location: error?a=creationfailed');
    }
}

if(isset($_POST['login']))
{
    $userName = $_POST['username'];
    $userPass = $_POST['pass'];
    $login = $account->accountLogin($userName,$userPass);
    if($login == "OK")
    {
        header('location: index');
    }
    else 
    {
        header('location: error?a=invalidpass');
    }
}

?>

<!-- Image header -->
<div class="w3-display-container w3-container">
    <img src="https://ak7.picdn.net/shutterstock/videos/2034937/thumb/1.jpg" alt="Jeans" style="width:100%;height:200px">
    <div class="w3-display-topleft w3-text-white" style="padding:24px 48px">
        <h1>Your account</h1>
    </div>
</div>

<div class="w3-display-container w3-container w3-center">
    <hr/>
</div>

<?php 
include("$docroot/templates/profile/login.php");
?>  


<?php 
include("$docroot/templates/footer.php");
?>  