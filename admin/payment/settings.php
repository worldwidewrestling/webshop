<?php 
$page = "Admin Dashboard";
include('../templates/header.php'); 

// Order controller
include('../controllers/OrderController.php');
$orderClass = new Orders;

// Product controller
include('../controllers/ProductController.php');
$productClass = new Products;

include('../../controller/mail.php');
$mailClass = new Mail;


// Payment controller
include('../controllers/PaymentController.php');
$paymentClass = new Payment;
$getCredentials = $paymentClass->getCredentials();

?>

<?php 
include('../templates/payment/settings.php');
?>

<?php include('../templates/footer.php'); ?>