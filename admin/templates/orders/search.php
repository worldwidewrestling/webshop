<?php 

// Execute the functions to get information


?>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>All orders</h2>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <table class="w3-table">

            <thead>
                <th>Ordered by</th>
                <th>Order price</th>
                <th>Order product</th>
            </thead>

            <tbody>
                <?php
                    if($searchQuery == 'finished')
                    {
                        $query = "SELECT * FROM `orders` WHERE `status` = '3'";
                        $sql = $con->query($query);
                        while($row = $sql->fetch_assoc())
                        {

                            $orderedBy = $row['name'];
                            $orderPrice = number_format($row['price'], 2);
                            $orderProduct = $row['product'];
                            $orderId = $row['id'];

                            echo '
                            <tr>
                                <td>'.$orderedBy.'</td>
                                <td>'.$orderPrice.'</td>
                                <td>'.$orderProduct.'</td>
                                <td><a href="/admin/orders/view-order?order='.$orderId.'"><button class="w3-button w3-small w3-yellow w3-hover-black">View</button></a>
                            ';
                        }
                    }
                    if($searchQuery == 'unpaid')
                    {
                        $query = "SELECT * FROM `orders` WHERE `status` = '1'";
                        $sql = $con->query($query);
                        while($row = $sql->fetch_assoc())
                        {

                            $orderedBy = $row['name'];
                            $orderPrice = number_format($row['price'], 2);
                            $orderProduct = $row['product'];
                            $orderId = $row['id'];

                            echo '
                            <tr>
                                <td>'.$orderedBy.'</td>
                                <td>'.$orderPrice.'</td>
                                <td>'.$orderProduct.'</td>
                                <td><a href="/admin/orders/view-order?order='.$orderId.'"><button class="w3-button w3-small w3-yellow w3-hover-black">View</button></a>
                            ';
                        }
                    }
                    if($searchQuery == 'production')
                    {
                        $query = "SELECT * FROM `orders` WHERE `status` = '2'";
                        $sql = $con->query($query);
                        while($row = $sql->fetch_assoc())
                        {

                            $orderedBy = $row['name'];
                            $orderPrice = number_format($row['price'], 2);
                            $orderProduct = $row['product'];
                            $orderId = $row['id'];

                            echo '
                            <tr>
                                <td>'.$orderedBy.'</td>
                                <td>'.$orderPrice.'</td>
                                <td>'.$orderProduct.'</td>
                                <td><a href="/admin/orders/view-order?order='.$orderId.'"><button class="w3-button w3-small w3-yellow w3-hover-black">View</button></a>
                            ';
                        }
                    }
                ?>
            </tbody>

        </table>

    </div>  
</div>
