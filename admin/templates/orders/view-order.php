<?php 

// Execute the functions to get information
$getOrder = $orderClass->getOrder($orderId);

if(isset($_POST['submit']))
{
    $orderId = $_POST['id'];
    $userMail = $_POST['mail'];
    $userName = $_POST['name'];

    $sendShipment = $mailClass->sendShipment($userMail,$userName,$orderId);
    $shipOrder = $orderClass->shipOrder($orderId);

}

if(isset($_POST['remind']))
{
    $orderId = $_POST['id'];
    $userMail = $_POST['mail'];
    $userName = $_POST['name'];

    $sendReminder = $mailClass->sendPaymentReminder($userMail,$userName,$orderId);

}

?>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>Order info</h2>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <table class="w3-table">
            <tr>
                <td><strong>Product</strong></td>
                <td><?php echo $getOrder['product']; ?></td>
            </tr>
            <tr>
                <td><strong>Size</strong></td>
                <td><?php echo $getOrder['size']; ?></td>
            </tr>
        </table>

    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <div class="w3-half">
            <h3>Recipient info</h3>
            <table class="w3-table">
                <tr>
                    <td><strong>Is a guest order?</strong></td>
                    <td><?php if($getOrder['client'] == NULL){ echo "Yes"; } else { echo "No"; } ?></td>
                </tr>
                <tr>
                    <td><strong>Name</strong></td>
                    <td><?php echo $getOrder['name']; ?></td>
                </tr>
                <tr>
                    <td><strong>Address</strong></td>
                    <td><?php echo $getOrder['address']; ?></td>
                </tr>
                <tr>
                    <td><strong>City</strong></td>
                    <td><?php echo $getOrder['city']; ?></td>
                </tr>
                <tr>
                    <td><strong>Postal Code</strong></td>
                    <td><?php echo $getOrder['zip']; ?></td>
                </tr>
                <tr>
                    <td><strong>State</strong></td>
                    <td><?php echo $getOrder['state']; ?></td>
                </tr>
                <tr>
                    <td><strong>Country</strong></td>
                    <td><?php echo $getOrder['country']; ?></td>
                </tr>
            </table>
        </div>

        <div class="w3-half">
            <h3>Order status</h3>
            <?php 
            
                $orderStatus = $getOrder['status'];

                if($orderStatus == '1')
                {
                    $orderStatus = "Awaiting payment";
                }
                if($orderStatus == '2')
                {
                    $orderStatus = "Waiting for fulfilment";
                }
                if($orderStatus == '3')
                {
                    $orderStatus = "Shipped";
                }

            ?>
            <p><?php echo $orderStatus; ?></p>
                <?php 
                if($orderStatus == "Waiting for fulfilment")
                { ?>
                <form method="post">
                    <input type="hidden" name="id" value="<?php echo $getOrder['id'];?>">
                    <input type="hidden" name="mail" value="<?php echo $getOrder['mail'];?>">
                    <input type="hidden" name="name" value="<?php echo $getOrder['name']; ?>">
                    <input type="submit" class="w3-button w3-yellow w3-hover-black" value="Ship this order" name="submit">
                </form>
                <?php 
                }
                ?>

                <?php 
                if($orderStatus == "Awaiting payment")
                { ?>
                <form method="post">
                    <input type="hidden" name="id" value="<?php echo $getOrder['id'];?>">
                    <input type="hidden" name="mail" value="<?php echo $getOrder['mail'];?>">
                    <input type="hidden" name="name" value="<?php echo $getOrder['name']; ?>">
                    <input type="submit" class="w3-button w3-yellow w3-hover-black" value="Send reminder" name="remind">
                </form>
                <?php 
                }
                ?>
        </div>

    </div>  
</div>