<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h3>You are currently not logged in</h3>
        <p>To get access to all of the admin tools, it is required that you login.</p>

        <form method="post" action="/action/adminlogin">
            <input type="email" name="mail" placeholder="Your email address" class="w3-input w3-margin-bottom">
            <input type="password" name="pass" placeholder="Your password" class="w3-input w3-margin-bottom">
            <input type="submit" class="w3-button w3-margin-bottom w3-yellow w3-hover-black" value="Login">
        </form>

    </div>
</div>