<?php 

// Execute the functions to get information
$totalRevenue = $orderClass->getTotalRevenue();
$totalOrderAmount = $orderClass->getOrderCount();
$totalUnpaidOrders = $orderClass->getUnpaidOrders();
$totalWaitingShipment = $orderClass->getWaitingShipment();

?>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>Dashboard</h2>

        <hr/>

        <div class="w3-quarter w3-blue w3-margin-bottom">
            <h3>Total profit</h3>
            <h4>$ <?php echo $totalRevenue; ?></h4>
        </div>

    <a href="/admin/orders/search?query=finished">
        <div class="w3-quarter w3-green w3-hover-yellow w3-margin-bottom">
            <h3>All finished orders</h3>
            <h4><?php echo $totalOrderAmount; ?></h4>
        </div>
    </a>

    <a href="/admin/orders/search?query=unpaid">
        <div class="w3-quarter w3-red w3-hover-yellow w3-margin-bottom">
            <h3>Unpaid orders</h3>
            <h4><?php echo $totalUnpaidOrders; ?></h4>
        </div>
    </a>

    <a href="/admin/orders/search?query=production">
        <div class="w3-quarter w3-orange w3-hover-yellow w3-text-white w3-margin-bottom">
            <h3>To deliver</h3>
            <h4><?php echo $totalWaitingShipment; ?></h4>
        </div>
    </a>

    </div>  
</div>

<hr/>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <div class="w3-half">
            <h3>Popular products</h3>
            <table class="w3-table">
                <thead>
                    <th>Rank</th>
                    <th>Name</th>
                    <th>Amount sold</th>
                </thead>
                <tbody>
                    <?php 
                        $i = 1;
                        $query = "SELECT * FROM `products` ORDER BY `ordercount` DESC LIMIT 5";
                        $sql = $con->query($query);
                        while($row = $sql->fetch_assoc())
                        {
                            $productName = $row['name'];
                            $productOrders = $row['ordercount'];

                            echo '
                                <tr>
                                    <td>'.$i++.'</td>
                                    <td>'.$productName.'</td>
                                    <td>'.$productOrders.'</td>
                            ';

                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="w3-half">
            <h3>Best customers</h3>
            <table class="w3-table">
                <thead>
                    <th>Rank</th>
                    <th>Name</th>
                    <th>Orders</th>
                </thead>
                <tbody>
                    <?php 

                        $i = 1;
                        $query = "SELECT `name`, COUNT(`name`) AS MOST_FREQUENT from `orders` GROUP BY `name` ORDER BY COUNT(`name`) DESC LIMIT 5";
                        $sql = $con->query($query);
                        while($row = $sql->fetch_assoc())
                        {
                            $productName = $row['name'];
                            $productOrders = $row['MOST_FREQUENT'];

                            echo '
                                <tr>
                                    <td>'.$i++.'</td>
                                    <td>'.$productName.'</td>
                                    <td>'.$productOrders.'</td>
                            ';

                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>  
</div>

