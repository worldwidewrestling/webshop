<?php 

// Execute the functions to get information
$getBestSeller = $productClass->getBestSeller();
$getNewest = $productClass->getNewest();

?>

<div class="w3-display-container w3-container w3-center">

        <?php
            $query = "SELECT * FROM `products` WHERE `active` = '0' ORDER BY `id` ASC";
            $sql = $con->query($query);
            while($row = $sql->fetch_assoc())
            {
                $productName = $row['name'];
                $productPrice = $row['price'];
                $productDescription = $row['description'];
                $productOrders = $row['ordercount'];
                $productImage = $row['image'];
                $productId = $row['id'];

                echo '
                <div class="w3-row">
                    <div class="w3-half">
                        <h4>'.$productName.'</h4>
                        <p>'.$productDescription.'</p>
                    </div>
                    <div class="w3-half">
                        <img src="/images/'.$productImage.'" class="w3-image" style="width:200px;height:auto">
                        <br/><br/>
                        <a href="/action/acceptproduct?id='.$productId.'"><button class="w3-button w3-green w3-small">Approve</button></a>
                        <a href="/action/declineproduct?id='.$productId.'"><button class="w3-button w3-red w3-small">Decline</button></a>
                    </div>
                ';

            }
        ?>
  
</div>
