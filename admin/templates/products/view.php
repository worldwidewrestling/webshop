<?php 

// Execute the functions to get information
$getProduct = $productClass->getProduct($productId);
$getAmountSpotlight = $productClass->getAmountSpotlight();

if(isset($_POST['remove']))
{
    $removeSpotlight = $productClass->removeSpotlight($productId);
}

if(isset($_POST['submit']))
{
    $setSpotlight = $productClass->setSpotlight($productId);
}

?>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>Product info</h2>
    </div>  
</div>

<hr/>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <img class="w3-image" src="/images/<?php echo $getProduct['image']; ?>" style="width:300px">

        <table class="w3-table">
            <tr>
                <td><strong>Product name</strong></td>
                <td><?php echo $getProduct['name']; ?></td>
            </tr>
            <tr>
                <td><strong>Times ordered</strong></td>
                <td><?php echo $getProduct['ordercount']; ?> </td>
            </tr>
            <tr>
                <td><strong>Description</strong></td>
                <td><?php echo $getProduct['description']; ?></td>
            </tr>
        </table>

    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <?php 
            if($getAmountSpotlight < '2')
            {
                $getSpotlight = $productClass->getSpotlight($productId);
                if($getSpotlight != '1')
                {
                    echo '
                    <form method="post">
                        <input type="hidden" name="product" value="'.$productId.'">
                        <input type="submit" class="w3-button w3-black w3-hover-yellow" value="Make spotlight" name="submit">
                    </form>
                    ';
                }
                else 
                {
                    echo '<p>This product is currently a spotlight product</p>';
                    echo '
                    <form method="post">
                        <input type="hidden" name="product" value="'.$productId.'">
                        <input type="submit" class="w3-button w3-black w3-hover-yellow" value="Remove spotlight" name="remove">
                    </form>
                    ';
                }
            }
            else 
            {
                $getSpotlight = $productClass->getSpotlight($productId);
                if($getSpotlight != '1')
                {
                    echo '
                    <p>You can not make this product a spotlight product, because there can only be a maximum of two spotlight products at the same time</p>
                    ';
                }
                else 
                {
                    echo '<p>This product is currently a spotlight product</p>';
                    echo '
                    <form method="post">
                        <input type="hidden" name="product" value="'.$productId.'">
                        <input type="submit" class="w3-button w3-black w3-hover-yellow" value="Remove spotlight" name="remove">
                    </form>
                    ';
                } 
            }
        ?>

    </div>  
</div>

<hr/>
