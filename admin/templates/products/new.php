
<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>New product</h2>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <form method="post" action="/action/createproduct">
            <input type="text" class="w3-input w3-margin-bottom" name="name" placeholder="Product name">
            <textarea name="description" class="w3-input w3-margin-bottom" placeholder="Product description"></textarea>
            <input type="text" class="w3-input w3-margin-bottom" name="price" value="21.99" readonly>
            <input type="submit" class="w3-button w3-margin-bottom w3-yellow w3-hover-black" value="Submit">
        </form>

    </div>  
</div>
