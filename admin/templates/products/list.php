<?php 

// Execute the functions to get information
$getBestSeller = $productClass->getBestSeller();
$getNewest = $productClass->getNewest();

?>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>General Statistics</h2>
    </div>  
</div>

<hr/>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <div class="w3-half w3-blue">
            <h3>Best-selling product</h3>
            <h4><?php echo $getBestSeller; ?></h4>
        </div>

        <div class="w3-half w3-green">
            <h3>Newest product</h3>
            <h4><?php echo $getNewest; ?></h4>
        </div>

    </div>  
</div>

<hr/>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>All products</h2>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <table class="w3-table">

            <thead>
                <th>Product name</th>
                <th>Product price</th>
                <th>Product orders</th>
            </thead>

            <tbody>
                <?php
                    $query = "SELECT * FROM `products` ORDER BY `id` ASC";
                    $sql = $con->query($query);
                    while($row = $sql->fetch_assoc())
                    {
                        $productName = $row['name'];
                        $productPrice = $row['price'];
                        $productOrders = $row['ordercount'];
                        $productId = $row['id'];

                        echo '
                        <tr>
                            <td>'.$productName.'</td>
                            <td>'.$productPrice.'</td>
                            <td>'.$productOrders.'</td>
                            <td><a href="/admin/products/view-product?product='.$productId.'"><button class="w3-button w3-small w3-yellow w3-hover-black">View</button></a>
                        ';

                    }
                ?>
            </tbody>

        </table>

    </div>  
</div>
