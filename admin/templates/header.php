<?php 
session_start();
$docroot = $_SERVER['DOCUMENT_ROOT'];
include("$docroot/includes/config.php");
if($_SESSION['logged_in'] != 'true') {
  $login = "No";
} else {
  $login = "Yes";
  $user_id = $_SESSION['id'];
}

$query = "SELECT * FROM `admins` WHERE `id` = '$user_id'";
$sql = $con->query($query);
while($row = $sql->fetch_assoc())
{
  $adminPayments = $row['payment'];
  $adminMails = $row['mails'];
  $adminSupport = $row['support'];
}

?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo "$page"; ?></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/www.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.w3-sidebar a {font-family: "Roboto", sans-serif}
body,h1,h2,h3,h4,h5,h6,.w3-wide {font-family: "Montserrat", sans-serif;}

* {box-sizing: border-box}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

/* Style the search bar */
input[class=search] {
  width: 130px;
  border: none;
  border-bottom: 1px solid #ccc;
  padding: 12px 20px 12px 40px;
  background-image: url('https://www.w3schools.com/howto/searchicon.png');
  background-position: 10px 12px;
  background-repeat: no-repeat;
  -webkit-transition: width 0.4s ease-in-out;
  transition: width 0.4s ease-in-out;
}

/* When the input field gets focus, change its width to 100% */
input[class=search]:focus {
  width: 100%;
}

</style>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-5641317074082353",
          enable_page_level_ads: true
     });
</script>
</head>
<body class="w3-content" style="max-width:1200px">

<!-- Navigation area --> 

<!-- Top menu on small screens -->
<header class="w3-bar w3-top w3-hide-large w3-black w3-xlarge">
  <div class="w3-bar-item w3-padding-24 w3-wide"><h3>3DUB Admin</h3></div>
  <a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding-24 w3-right" onclick="w3_open()"><i class="fa fa-bars"></i></a>
</header>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-bar-block w3-white w3-collapse w3-top" style="z-index:3;width:250px" id="mySidebar">
  <div class="w3-container w3-display-container w3-padding-16">
    <i onclick="w3_close()" class="fa fa-remove w3-hide-large w3-button w3-display-topright"></i>
    <h3 class="w3-wide"><b><img src="/images/logo.png" style="width:50%;height:auto"/></b></h3>
  </div>
  <div class="w3-large w3-text-grey" style="font-weight:bold">

    <?php 
      if($login == 'No') {
        echo '<a href="/admin/login" class="w3-bar-item w3-button">Login</a>';
      }elseif($login == 'Yes') {
        echo '<a href="/admin/index" class="w3-bar-item w3-button">Home</a>';
        if($adminPayments == '1')
        {
          echo '<h4>Payments</h4>';
          echo '<a href="/admin/payment/settings" class="w3-bar-item w3-button">Payment settings</a>';
          echo '<hr>';
        }
        if($adminSupport == '1')
        {
          echo '<h4>Products</h4>';
          echo '<a href="/admin/products/list" class="w3-bar-item w3-button">Product list</a>';
          echo '<a href="/admin/products/new" class="w3-bar-item w3-button">New product</a>';
          echo '<a href="/admin/products/suggested" class="w3-bar-item w3-button">Suggested products</a>';
          echo '<hr>';
          echo '<h4>Orders</h4>';
          echo '<a href="/admin/orders/list" class="w3-bar-item w3-button">Order list</a>';
          echo '<a href="/admin/orders/donations" class="w3-bar-item w3-button">Max Love Orders</a>';
          echo '<hr>';
          echo '<h4>Customers</h4>';
          echo '<a href="/admin/customers/list" class="w3-bar-item w3-button">Customer list</a>';
          echo '<hr>';
        }
        if($adminMails == '1')
        {
          echo '<h4>Other Settings</h4>';
          echo '<a href="/admin/users/list" class="w3-bar-item w3-button">User list</a>';
          echo '<hr>';
        }
        echo '<a href="/admin/logout" class="w3-bar-item w3-button">Logout</a>';
      }
    ?>

  </div>
</nav>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:250px">

  <!-- Push down content on small screens -->
  <div class="w3-hide-large" style="margin-top:120px"></div>

      <h1><?php echo $action; ?></h1>
