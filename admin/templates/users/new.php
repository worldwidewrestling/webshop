<?php 

// Execute the functions to get information
if(isset($_POST['submit']))
{
    $adminMail = $_POST['mail'];
    $adminPass = $_POST['pass'];
    $adminPermission = $_POST['permission'];

    $createAdmin = $adminClass->create($adminMail,$adminPass,$adminPermission);
    if($createAdmin)
    {
        header('location: ../index');
    }

}


?>
<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>New admin</h2>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <form method="post">
            <input type="email" class="w3-input w3-margin-bottom" name="mail" placeholder="Email address">
            <input type="password" class="w3-input w3-margin-bottom" name="pass" placeholder="Password" autocomplete="no">
            <select name="permission">
                <option value="mails">Administrator</option>
                <option value="support">Support</option>
            </select>
            <input type="submit" class="w3-button w3-yellow w3-hover-black" name="submit" value="Submit">
        </form>

    </div>  
</div>
