<?php 

// Execute the functions to get information

?>
<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>All admins</h2>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <a href="new"><button class="w3-button w3-blue">New admin</button></a>
        <table class="w3-table">

            <thead>
                <th>Email address</th>
            </thead>

            <tbody>
                <?php
                    $query = "SELECT * FROM `admins` ORDER BY `id` ASC";
                    $sql = $con->query($query);
                    while($row = $sql->fetch_assoc())
                    {
                        $productName = $row['email'];
                        echo '
                        <tr>
                            <td>'.$productName.'</td>
                        ';

                    }
                ?>
            </tbody>

        </table>

    </div>  
</div>
