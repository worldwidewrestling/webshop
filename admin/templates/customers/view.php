<?php 

// Execute the functions to get information
$clientId = $_GET['id'];
include('../controllers/ClientController.php');
$clientClass = new Client;
$getClient = $clientClass->getClient($clientId);

$getOrderAmountCustomer = $orderClass->getOrderAmountCustomer($clientId);
$getClientRevenue = $orderClass->getClientRevenue($clientId);

?>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <div class="w3-third w3-blue w3-margin-bottom">
            <h3>Customer revenue</h3>
            <h4>$ <?php echo $getClientRevenue; ?></h4>
        </div>
    <a href="#orders">
        <div class="w3-third w3-green w3-margin-bottom w3-text-white">
            <h3>Customer orders</h3>
            <h4><?php echo $getOrderAmountCustomer; ?></h4>
        </div>
    </a>
    <a href="#points">
        <div class="w3-third w3-orange w3-margin-bottom w3-text-white">
            <h3>Customer points</h3>
            <h4><?php echo $getClient['points']; ?></h4>
        </div>
    </a>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2><?php echo $getClient['name']; ?></h2>
        <table class="w3-table">
            <tr>
                <td><strong>Email address</strong></td>
                <td><?php echo $getClient['mail']; ?></td>
            </tr>
            <tr>
                <td><strong>Username</strong></td>
                <td><?php echo $getClient['username']; ?></td>
            </tr>
        </table>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <div class="w3-half">
            <h3>Client address</h3>
            <table class="w3-table">
                <tr>
                    <td><strong>Address</strong></td>
                    <td><?php echo $getClient['address']; ?></td>
                </tr>
                <tr>
                    <td><strong>City</strong></td>
                    <td><?php echo $getClient['city']; ?></td>
                </tr>
                <tr>
                    <td><strong>Postal code</strong></td>
                    <td><?php echo $getClient['zip']; ?></td>
                </tr>
                <tr>
                    <td><strong>State</strong></td>
                    <td><?php echo $getClient['state']; ?></td>
                </tr>
                <tr>
                    <td><strong>Country</strong></td>
                    <td><?php echo $getClient['country']; ?></td>
                </tr>
            </table>
        </div>
        <div class="w3-half" id="points">
            <h3>Customer vouchers</h3>
            <table class="w3-table">
                <?php
                    $query = "SELECT * FROM `coupons` WHERE `user_id` = '$clientId' LIMIT 5";
                    $sql = $con->query($query);
                    while($row = $sql->fetch_assoc())
                    {
                        $voucherId = $row['coupon_id'];
                        
                        echo '
                        <tr>
                            <td><strong>Voucher</strong></td>
                            <td>'.$voucherId.'</td>
                        ';

                    }
                ?>
            </table>
        </div>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center" id="orders">
    <div class="w3-row">
        <h3>Customer orders</h3>
        <?php 
            if($getOrderAmountCustomer == '0')
            {
        ?>
        <p>This customer has not placed any orders yet</p>

        <?php 
            } else 
            { ?>

        <table class="w3-table">
            <thead>
                <th>Order ID</th>
                <th>Order price</th>
                <th>Order status</th>
            </thead>
            <tbody>
                <?php 
                    $query2 = "SELECT * FROM `orders` WHERE `client` = '$clientId'";
                    $sql2 = $con->query($query2);
                    while($row2 = $sql2->fetch_assoc())
                    {
                        $orderId = $row2['id'];
                        $orderPrice = $row2['price'];
                        $orderStatus = $row2['status'];

                        if($orderStatus == '1')
                        {
                            $orderStatus = "Awaiting payment";
                        }
                        elseif($orderStatus == '2')
                        {
                            $orderStatus = "Awaiting shipment";
                        }
                        elseif($orderStatus == '3')
                        {
                            $orderStatus = "Shipped";
                        }

                        echo '
                        <tr>
                            <td>'.$orderId.'</td>
                            <td>'.$orderPrice.'</td>
                            <td>'.$orderStatus.'</td>
                            <td><a href="/admin/orders/view-order?order='.$orderId.'"><button class="w3-button w3-small w3-yellow w3-hover-black">View order</button></a></td>
                        ';

                    }
                ?>
            </tbody>
        </table>

        <?php
            }
        ?>
    
    </div>  
</div>