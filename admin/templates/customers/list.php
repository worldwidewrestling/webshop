<?php 

// Execute the functions to get information


?>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>All Customers</h2>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <table class="w3-table">
        <form method="post">
            <input type="text" class="search" placeholder="Customer email">
            <input type="submit" class="w3-button w3-yellow w3-hover-black w3-small w3-margin-bottom w3-margin-top" value="Search" name="search">
        </form>
            <thead>
                <th>Customer name</th>
                <th>Customer mail</th>
                <th>Customer loyalty points</th>
            </thead>

            <tbody>
                <?php
                    $query = "SELECT * FROM `users`";
                    $sql = $con->query($query);
                    while($row = $sql->fetch_assoc())
                    {
                        $customerName = $row['name'];
                        $customerMail = $row['mail'];
                        $customerPoints = $row['points'];
                        $customerId = $row['id'];

                        echo '
                        <tr>
                            <td>'.$customerName.'</td>
                            <td>'.$customerMail.'</td>
                            <td>'.$customerPoints.'</td>
                            <td><a href="/admin/customers/view?id='.$customerId.'"><button class="w3-button w3-yellow w3-hover-black">View</button></a>
                        ';

                    }
                ?>
            </tbody>

        </table>

    </div>  
</div>
