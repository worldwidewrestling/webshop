<?php 

if(isset($_POST['submitsandbox']))
{
    $clientID = $_POST['app'];
    $appSecret = $_POST['secret'];
    $appToken = $_POST['token'];

    $updateCredentials = $paymentClass->updateCredentials("sandbox",$clientID,$appSecret,$appToken);
}

if(isset($_POST['submitlive']))
{
    $clientID = $_POST['app'];
    $appSecret = $_POST['secret'];
    $appToken = $_POST['token'];

    $updateCredentials = $paymentClass->updateCredentials("live",$clientID,$appSecret,$appToken);
}

?>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">
        <h2>Paypal Settings</h2>
    </div>  
</div>

<div class="w3-display-container w3-container w3-center">
    <div class="w3-row">

        <div class="w3-half">
            <h3 class="w3-text-yellow">Sandbox Settings</h3>
            <form method="post">
                <p>Client ID</p>
                <input type="text" class="w3-input" name="app" value="<?php echo $getCredentials['sandbox']; ?>">
                <p>APP Secret</p>
                <input type="text" class="w3-input" name="secret" value="<?php echo $getCredentials['sandbox_secret']; ?>">
                <p>API Token</p>
                <input type="text" class="w3-input w3-margin-bottom" name="token" value="<?php echo $getCredentials['token_sandbox']; ?>" readonly>
                <input type="submit" class="w3-button w3-yellow w3-hover-black w3-margin-bottom" name="submitsandbox" value="Change credentials">
            </form>
        </div>

        <div class="w3-half">
            <h3 class="w3-text-yellow">Live Settings</h3>
            <form method="post">
                <p>Client ID</p>
                <input type="text" class="w3-input" name="app" value="<?php echo $getCredentials['live']; ?>">
                <p>APP Secret</p>
                <input type="text" class="w3-input" name="secret" value="<?php echo $getCredentials['live_secret']; ?>">
                <p>API Token</p>
                <input type="text" class="w3-input w3-margin-bottom" name="token" value="<?php echo $getCredentials['token']; ?>" readonly>
                <input type="submit" class="w3-button w3-yellow w3-hover-black w3-margin-bottom" name="submitlive" value="Change credentials">
            </form>
        </div>

    </div>  
</div>
