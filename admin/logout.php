<?php

session_start();
unset($_SESSION['logged_in']);
unset($_SESSION['id']);
session_destroy();
header('location: ../admin/index');

?>