<?php 
$page = "Admin Dashboard";
include('../templates/header.php'); 

// Order controller
include('../controllers/OrderController.php');
$orderClass = new Orders;
$getMaxloveDonations = $orderClass->getMaxloveDonations();
$getMaxloveRevenue = $getMaxloveDonations * 2.50;
$showDonations = number_format($getMaxloveRevenue, 2);

// Product controller
include('../controllers/ProductController.php');
$productClass = new Products;

?>

<?php 
include('../templates/orders/donations.php');
?>

<?php include('../templates/footer.php'); ?>