<?php 
$orderId = $_GET['order'];
$page = "Admin Dashboard";
include('../templates/header.php'); 

// Order controller
include('../controllers/OrderController.php');
$orderClass = new Orders;

// Product controller
include('../controllers/ProductController.php');
$productClass = new Products;

include('../../controller/mail.php');
$mailClass = new Mail;

?>

<?php 
include('../templates/orders/view-order.php');
?>

<?php include('../templates/footer.php'); ?>