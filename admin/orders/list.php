<?php 
$page = "Admin Dashboard";
include('../templates/header.php'); 

// Order controller
include('../controllers/OrderController.php');
$orderClass = new Orders;

// Product controller
include('../controllers/ProductController.php');
$productClass = new Products;

?>

<?php 
include('../templates/orders/list.php');
?>

<?php include('../templates/footer.php'); ?>