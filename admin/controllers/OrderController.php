<?php 

class Orders 
{
    function getTotalRevenue()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT SUM(price) FROM `orders` WHERE `status` >= '2'";
        $sql = $con->query($query);
        $allRows = mysqli_num_rows($sql);
        while($row = $sql->fetch_assoc())
        {
            $totalRevenue = $row['SUM(price)'];
            if($totalRevenue == NULL)
            {
                $revenueReturnTotal = "0.00";
            }
            else 
            {
                $expenses = $allRows * 16;

                if($allRows == 0 )
                {
                    $expenses = 0  ;
                }
    
                $revenueReturn = $totalRevenue - $expenses;
                $revenueReturnTotal = number_format($revenueReturn, 2);
            }
    
            return $revenueReturnTotal;

        }
    }

    function getOrderCount()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `orders` WHERE `status` = '3'";
        $sql = $con->query($query);
        $rows = $sql->num_rows;
        return $rows;
    }

    function getUnpaidOrders()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `orders` WHERE `status` = '1'";
        $sql = $con->query($query);
        $rows = $sql->num_rows;
        return $rows;
    }

    function getWaitingShipment()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `orders` WHERE `status` = '2'";
        $sql = $con->query($query);
        $rows = $sql->num_rows;
        return $rows;
    }

    function getOrder($orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `orders` WHERE `id` = '$orderId'";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            return $row;
        }
    }

    function shipOrder($orderId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "UPDATE `orders` SET `status` = '3' WHERE `id` = '$orderId'";
        $sql = $con->query($query);
    }

    function getOrderAmountCustomer($clientId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `orders` WHERE `client` = '$clientId'";
        $sql = $con->query($query);
        $rows = mysqli_num_rows($sql);
        return $rows;
    }

    function getClientRevenue($clientId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        $query = "SELECT SUM(price) FROM `orders` WHERE `client` = '$clientId'";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $customerRevenue = $row['SUM(price)'];
            $returnRevenue = number_format($customerRevenue, 2);
            return $returnRevenue;
        }
    }

    function getMaxloveDonations()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");

        $query = "SELECT * FROM `orders` WHERE `coupon` = 'FUCKCANCER'";
        $sql = $con->query($query);
        $rows = mysqli_num_rows($sql);
        return $rows;

    }

}

?>