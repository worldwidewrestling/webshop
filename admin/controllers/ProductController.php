<?php 

class Products
{
    function getBestSeller()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `products` ORDER BY `ordercount` DESC LIMIT 1";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $productName = $row['name'];

            return $productName;

        }
    }

    function getNewest()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `products` ORDER BY `id` DESC LIMIT 1";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $productName = $row['name'];

            return $productName;

        }
    }

    function getProduct($productId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `products` WHERE `id` = '$productId'";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            return $row;
        }
    }

    function getAmountSpotlight()
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `products` WHERE `spotlight` = '1'";
        $sql = $con->query($query);
        $rows = mysqli_num_rows($sql);
        return $rows;
    }

    function getSpotlight($productId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "SELECT * FROM `products` WHERE `id` = '$productId'";
        $sql = $con->query($query);
        while($row = $sql->fetch_assoc())
        {
            $isSpotlight = $row['spotlight'];
            
            return $isSpotlight;

        }
    }

    function removeSpotlight($productId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "UPDATE `products` SET `spotlight` = '0' WHERE `id` = '$productId'";
        $sql = $con->query($query);
    }

    function setSpotlight($productId)
    {
        $docroot = $_SERVER['DOCUMENT_ROOT'];
        include("$docroot/includes/config.php");
        $query = "UPDATE `products` SET `spotlight` = '1' WHERE `id` = '$productId'";
        $sql = $con->query($query);
    }

}

?>