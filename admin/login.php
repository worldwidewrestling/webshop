<?php 

$page = "Admin Dashboard";
include('templates/header.php'); 

?>

<?php 
    if($login == "No")
    {
        include('templates/profile/login.php');
    }
    elseif($login == "Yes")
    {
        include('templates/general/dashboard.php');
    }
?>

<?php include('templates/footer.php'); ?>